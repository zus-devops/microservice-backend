# REST version of Spring PetClinic Sample Application (spring-framework-petclinic extend ) 

This backend version of the Spring Petclinic application only provides a REST API. 

The [spring-petclinic-angular project](https://github.com/spring-petclinic/spring-petclinic-angular) is a 
Angular front-end application which consumes the REST API.

Docker: openjdk:11-jre-slim

url servicio:http://159.89.34.221:8080/petclinic/
sonar: http://142.93.10.48:9000/projects?sort=-analysis_date
artifact: http://142.93.10.48:8082/ui/repos/tree/General/pet-clinic-releases%2Forg%2Fspringframework%2Fsamples%2Fspring-petclinic-rest%2F2.4.2%2Fspring-petclinic-rest-2.4.2.jar

despliegue en registry de docker: https://hub.docker.com/repository/docker/jsantos92/spring-petclinic-rest